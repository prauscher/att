<?php
/**
 * @ignore
 */
class DataBase
{
	private $db;
	/**
	 * @ignore
	 */
	public function DataBase($db = NULL, $utf8 = true)
	{
		if(defined("MYSQL_HOST"))
		{
			$this->db = new PDO("mysql:host=".MYSQL_HOST.";dbname=".MYSQL_DB."", MYSQL_USER, MYSQL_PASS);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			if($utf8)
			{
				$this->db->query("SET CHARACTER SET utf8;");
				$this->db->query("SET NAMES utf8;");
			}
		}
		else 
		{
			throw new Exception("No MYSQL Data defined");
		}
	}
	/**
	 * @ignore
	 */
	public function insert($statment, $data, $database = NULL)
	{
		try {
			if($database!=NULL)
			{
				$this->db->query("use ".$database);
			}
			$STH = $this->db->prepare($statment);
			$STH->execute($data);
			if($database!=NULL)
			{
				$this->db->query("use ".MYSQL_DB);
			}
			$STH->CloseCursor();
		}
		catch(PDOException $e) {
			echo "<pre>".$e."</pre>";
		}
	}
	/**
	 * @ignore
	 */
	public function insertID($statment, $data, $database = NULL)
	{
		try {
			if($database!=NULL)
			{
				$this->db->query("use ".$database);
			}
			$STH = $this->db->prepare($statment);
			$STH->execute($data);
			$STH->CloseCursor();
			$id = $this->query("SELECT LAST_INSERT_ID();", array());
			if($database!=NULL)
			{
				$this->db->query("use ".MYSQL_DB);
			}
			return $id[0][0];
		}
		catch(PDOException $e) {
			echo "<pre>".$e."</pre>";
		}
	}
	/**
	 * @ignore
	 */
	public function query($statment, $data, $database = NULL)
	{
		try {
			if($database!=NULL)
			{
				$this->db->query("use ".$database);
			}
			$STH = $this->db->prepare($statment);
			$STH->execute($data);
			if($database!=NULL)
			{
				$this->db->query("use ".MYSQL_DB);
			}
			$re = array();
			while($row = $STH->fetch()) {
				$re[] = $row;
			}
			$STH->CloseCursor();
			return $re;
		}
		catch(PDOException $e) {
			echo "<pre>".$e."</pre>";
		}
	}
	/**
	 * @ignore
	 */
	public function insertUpdate($database, $data, $where)
	{
		$sqlData = array();
		$sql = "UPDATE ".mysql_escape_string($database)." SET ";
		$firstdata = false;
		foreach($data as $key => $value)
		{
			if($firstdata){ $sql .=", "; } else { $firstdata = true; }
			$sql.= "`".mysql_escape_string($key)."` = ?";
			$sqlData[]=$value;
		}
		$sql .= " WHERE ";
		$firstdata = false;
		foreach($where as $key => $value)
		{
			if($firstdata){
				$sql .=", ";
			} 
			else
			{ 
				$firstdata = true;
			}
			$sql.= mysql_escape_string($key)." = ?";
			$sqlData[]=$value;
		}
		echo $sql;
		$this->insert($sql, $sqlData);
	}
	
}
?>