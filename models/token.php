<?php
include_once("../models/database.php");
class tokenModel extends DataBase
{
	static public $token = null;
	static public $antrag = null;
	static public $nutzer = null;
	static public function create()
	{
		tokenModel::$token=$_SESSION["token"];
	}
	static public function setToken($token)
	{
		$_SESSION["token"] = $token;
		
		tokenModel::$token=$_SESSION["token"];
	}
	static public function getToken()
	{
		return tokenModel::$token;
	}
	static public function setAntrag($id)
	{
		tokenModel::$antrag = $id;
	}
	public function getNutzerName()
	{
		$sql = "SELECT * FROM `token` WHERE `token` LIKE ?";
		$res = $this->query($sql, array(tokenModel::$token));
		if(isset($res[0]["nutzer"]))
		{
			return $res[0]["nutzer"];
		}
		return false;
	}
	public function berechtigung()
	{
		$sql = "SELECT * FROM  `token` WHERE  `token` =  ? AND  `antrag` = ?";
		$res = $this->query($sql, array(tokenModel::$token, tokenModel::$antrag));
		//var_dump($res);
		if(isset($res[0]))
		{
			tokenModel::$nutzer = $res[0]["nutzer"];
			return true;
		}
		return false;
	}
	public function setTokenByUser($antragID=null)
	{
		if($antragID==null)
		{
			$antragID = tokenModel::$antrag;
		}
		if(isset($_SESSION["nutzerName"]))
		{
			$antragsModel = new antragsModel();
			$token = $antragsModel->getToken($_SESSION["nutzerName"], $antragID);
			if($token!=false)
			{
				tokenModel::setToken($token);			
			}
		}
	}
}
