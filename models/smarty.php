<?php
include_once("../models/token.php");
class smartyModel
{
	static private $smarty = null;
	static public $controller = null;
	static public $view = null;
	static public $render = true;
	static public function createSmarty()
	{
		smartyModel::$smarty = $smarty = new Smarty();
		smartyModel::$smarty->setCompileDir(pathOS."tmp/");
		smartyModel::$smarty->assign('pfad', path);
		smartyModel::$smarty->assign('session', $_SESSION);
	}
	static public function escapeArray($array)
	{
	  $newArray = array();
	  foreach($array as $key=>$value)
	  {
	     if(is_array($value))
	     {
	       $newArray[$key] = smartyModel::escapeArray($value);
	     }
	     else
	     {
	       $newArray[$key]=htmlspecialchars($value);
	     }
	  }
	  return $newArray;
	}
	static public function assign($name, $value, $noEncode = false)
	{
	  if(is_string($value)&&!$noEncode)
	  {
		smartyModel::$smarty->assign($name, htmlspecialchars($value));
	  }
	  else if(is_array($value)&&!$noEncode)
	  {
	    smartyModel::$smarty->assign($name, smartyModel::escapeArray($value));
	  }
	  else if($noEncode)
	  {
	    smartyModel::$smarty->assign($name, $value);
	  }
	  else
	  {
	    smartyModel::$smarty->assign($name, htmlspecialchars($value));
	  }
	}
	static public function display($file)
	{
		$tokenModel = new tokenModel();
		if($tokenModel->berechtigung())
		{
			smartyModel::$smarty->caching = false;
		}
		if(smartyModel::$render==true)
		{
			smartyModel::$smarty->display($file);
		}
	}
	static public function test()
	{
		return "OK";
	}
}
