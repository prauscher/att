{include file='../template/top.tpl'}


    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Anträge</h1>
        <p>Wilkommen auf dem Antragstool der Jungen Piraten. Hier kannst du Online dir die Anträge des Bundesvorstandes und einiger Landesvorstände einsehen und neue stellen.</p>
        <p><a href="{{$pfad}}antrag/neu" class="btn btn-primary btn-large">Neuen Antrag &raquo;</a></p>
      </div>
      <div id="row">
        <div class="span12">
          <h3>Die letzten Anträge:</h3>
          <table style="width:100%;">
            <tr>
              <th>Antrags Name</th>
              <th>Gliederung</th>
              <th>Status</th>
              <th>Info</th>
            </tr>
            {foreach from=$antrage item=details}
            <tr>
              <td><a href="{{$pfad}}antrag/antrag/{{$details["id"]}}">{{$details["name"]}}</a></td>
              <td>{{$details["gliederung"]}}</td>
              <td>{{$details["status"]}}</td>
              <td>{{$details["info"]}}</td>
            </tr>
            {/foreach}
          </table>
        </div>
      </div>
      <!-- Example row of columns -->
      <!--
      <div class="row">
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
       </div>
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
      </div>
      !-->

     
{include file='../template/bottom.tpl'}