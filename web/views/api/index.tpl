{include file='../template/top.tpl'}


    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      
      <div class="row">
        <div class="span3">
              <ul class="nav nav-list">
<li class="nav-header">List header</li>
<li class="active"><a href="#">Home</a></li>
</ul>
          
        </div>
        <div class="span9">
          <h2><a name="#antragGET">GET /api/antrag</a></h2>
            Liefert die letzten 20 Anträge zurück.<br><br>
            Parameter:
            <table class="table">
              <tr><th>Name</th><th>Beschreibung</th><th>Optional</th></tr>
              <tr><td>gliederung</td><td>Suchen nach Anträgen einer Gliederung</td><td>Ja</td></tr>
            </table>
            <br>
            Rückgabe: (json):<br>
            <pre>
              <tr><th>Name</th><th>Beschreibung</th><th>Typ</th></tr>
              <tr><td>status</td><td>Ob der Aufruf erfolgreich war</td><td>String</td></tr>
              <tr><td>code</td><td>Ob der Aufruf erfolgreich war</td><td>Int</td></tr>
              <tr><td>res
            </pre>

        </div>
      </div>
      <!-- Example row of columns -->
      <!--
      <div class="row">
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
       </div>
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
      </div>
      !-->

     
{include file='../template/bottom.tpl'}