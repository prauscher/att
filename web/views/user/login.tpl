{include file='../template/top.tpl'}


    <div class="container">

      <!-- Example row of columns -->
      <div class="row">
        <div class="span12">
          <h2>Login</h2>
	{if isset($login) && $login == false}
    <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">X</button>
    <strong>Fehler! </strong> Login fehlgeschlagen, bitte überprüfe deine Nutzerdaten.
    </div>
	{/if}
              <form class="form-horizontal" method="post">
    <div class="control-group">
    <label class="control-label" for="inputEmail">Username</label>
    <div class="controls">
    <input type="text" id="user" placeholder="Username" name="user">
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="inputPassword">Password</label>
    <div class="controls">
    <input type="password" id="pw" name="pw" placeholder="Password">
    </div>
    </div>
 <div class="control-group">
<div class="controls">
<button type="submit" class="btn">Sign in</button>
</div>
</div>
    </form>
        </div>
      </div> 

     
{include file='../template/bottom.tpl'}