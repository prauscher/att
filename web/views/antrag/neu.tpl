{include file='../template/top.tpl'}


    <div class="container">

      <!-- Example row of columns -->
      <div class="row">
        <div class="span12">
          <h2>Neuer Antrag</h2>
          Hier kannst du einen neuen Antrag an eine Gliederung der Jupis stellen.<hr>
        </div>
      </div>
      <div class="row">
        <div class="span12">
              <form class="form-horizontal" method="post">
    <div class="control-group">
    <label class="control-label" for="inputEmail">Antragsname</label>
    <div class="controls">
    <input type="text" style="width:400px;"  name="titel" value="">
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputPassword">Antragstext</label>
    <div class="controls">
    <textarea rows="3" style="width:400px;" name="text"></textarea>
    </div>
    </div>
   
    
    <div class="control-group">
    <label class="control-label" for="inputPassword">Antragsbegründung*</label>
    <div class="controls">
    <textarea rows="3" style="width:400px;" name="begruendung"></textarea>
    </div>
    </div>
    
        <div class="control-group">
    <label class="control-label" for="inputPassword">Budget* </label>
    <div class="controls">
        <div class="input-prepend input-append">
    <span class="add-on">EURO</span>
    <input class="span2" id="appendedPrependedInput" type="text" name="budget">
    <span class="add-on">.00</span>
    </div>
    </div>
    </div>
    
     <div class="control-group">
    <label class="control-label" for="inputPassword">Gliederung</label>
    <div class="controls">
    <select style="width:400px;" name="gliederung">
    <option>Bund</option>
    {foreach from=$gliederung item=glied}
        {if $glied != "Bund"} 
    	   <option>{$glied}</option>
        {/if}
    {/foreach}
    </select>
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputEmail">Antragssteller*</label>
    <div class="controls">
    <input type="text" id="user" style="width:400px; placeholder="Username" name="autor">
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputEmail">Mailadresse*</label>
    <div class="controls">
    <input type="text" id="user" style="width:400px; placeholder="Username" name="autorMail">
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputEmail">Spam</label>
    <div class="controls">
    {{$spam}}
    </div>
    </div>
    
 <div class="control-group">
<div class="controls">
<button type="submit" class="btn">Stellen</button>
<br><br>* Muss <b>nicht</b> ausgefüllt werden
</div>
</div>
    </form>
        </div>
      </div> 

     
{include file='../template/bottom.tpl'}