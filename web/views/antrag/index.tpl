{include file='../template/top.tpl'}


    <div class="container" >

    <table style="width:100%;" id="example" class="table table-striped">
    	<tr>
    		<th>Antrags Name</th>
    		<th>Gliederung</th>
    		<th>Status</th>
    		<th>Info</th>
    	</tr>
    	{foreach from=$antrage item=details}
    	<tr>
    		<td><a href="{{$pfad}}antrag/antrag/{{$details["id"]}}">{{$details["name"]}}</a></td>
    		<td>{{$details["gliederung"]}}</td>
    		<td>{{$details["status"]}}</td>
            <td>
                {if $details["info"] == "angenommen"}
                                       <a style="width:80px;cursor:text;" class="btn btn-success result active">Angenommen</a>
                                    
                 {elseif $details["info"] == "abgelehnt"}
                                        <a style="width:80px;cursor:text;" class="btn btn-danger result active">Abgelehnt</a>
                                    
                 {else}
                                        <a style="width:80px;cursor:text;" class="btn btn-warning result active">Abstimmung</a>
                {/if}
            </td>
    		<!--<td>{{$details["info"]}}</td>!-->
    	</tr>
    	{/foreach}
    </table>


{include file='../template/bottom.tpl'}
<script>
$(document).ready(function() {
    $('#example').dataTable();
    console.log("TEST");
} );
</script>