{include file='../template/top.tpl'}


    <div class="container">

     <div class="hero-unit">
        <h1>{{$antrag["name"]}}</h1>
        <p><b>Antrag:</b><br><pre style="font-size:20px;">{{$antrag["beschluss"]}}</pre></p>
		<hr>
		<p><b>Begründung:</b><br><pre style="font-size:20px;">{{$antrag["begruendung"]}}</pre></p>
		{if $berechtigung == true}
		<p>
			
			<div class="btn-group" data-toggle="buttons-radio">
              	<button type="button" voteName="" vote="ja" class="btn btn-success vote {if $personVoting[$nutzerName]=="ja"}active{/if}">Dafür</button>
				<button type="button" voteName="" vote="enthaltung" class="btn btn-warning vote {if $personVoting[$nutzerName]=="enthaltung"}active{/if}">Enthaltung</button>
				<button type="button" voteName="" vote="nein" class="btn btn-danger vote {if $personVoting[$nutzerName]=="nein"}active{/if}">Dagegen</button>
            </div>
		</p>

		{/if}
      </div>
	<div class="row">
	<div class="span9">
	<ul id="myTab" class="nav nav-tabs">
              <li  class="active"><a href="#kommentare" data-toggle="tab">Komentare</a></li>
              <li><a href="#beschluss" data-toggle="tab">Abgestimmt</a></li>
              {if $berechtigung}<li><a href="#abstimmung" data-toggle="tab">Abstimmen</a></li>{/if}
			  <li><a href="#log" data-toggle="tab">Beschluss-Log</a></li>
			  {if $berechtigung}<li><a href="#admin" data-toggle="tab">Beschluss-Treffen</a></li>{/if}
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade in active" id="kommentare">
                <p>Die Diskussion zu diesem Antrag findet im Forum statt:</p>
                <a href="https://forum.junge-piraten.de/viewthread.php?boardid={{$boardID}}&threadid={{$threadid}}">Forum</a>
              </div>
              <div class="tab-pane fade" id="beschluss">
                <p>
					<table class="table" style="width:50%;">
						<p>So wurde abgestimmt von:</p>
						<tr><th>Name</th><th>Abgestimmt</th></tr>
						{foreach from=$voting item=vote}
								
									{if $vote["voting"] == "ja"}
										<tr><td ><a href="https://wiki.junge-piraten.de/wiki/Benutzer:{{$vote["nutzerName"]}}">{{$vote["nutzerName"]}}</a></td><td><a style="width:70px;cursor:text;" class="btn btn-success result active">Dafür</a></td></tr>
									{/if}
									{if $vote["voting"] == "nein"}
										<tr><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:{{$vote["nutzerName"]}}">{{$vote["nutzerName"]}}</a></td><td><a style="width:70px;cursor:text;" class="btn btn-danger result active">Dagegen</a></td></tr>
									{/if}
									{if $vote["voting"] == "enthaltung"}
										<tr><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:{{$vote["nutzerName"]}}">{{$vote["nutzerName"]}}</a></td><td><a style="width:70px;cursor:text;" class="btn btn-warning result active">Enthaltung</a></td></tr>
									{/if}
						{/foreach}
					</table>
				</p>
              </div>
              {if $berechtigung}
              <div class="tab-pane fade" id="abstimmung">
              	<p>Hier kannst du im Namen von andere Vorstandsmitglieder abstimmen.</p>
              <table class="table">
              <tr><th>Name</th><th>Abstimmung</th></tr>
              {foreach from=$user item=u}
              		<tr>
              			<td>{{$u}}</td>
              			<td>
              				<div class="btn-group" data-toggle="buttons-radio">
              						<button type="button" voteName="{$u}" vote="ja" class="btn btn-success vote {if $personVoting[$u]=="ja"}active{/if}">Dafür</button>
									<button type="button" voteName="{$u}" vote="enthaltung" class="btn btn-warning vote {if $personVoting[$u]=="enthaltung"}active{/if}">Enthaltung</button>
									<button type="button" voteName="{$u}" vote="nein" class="btn btn-danger vote {if $personVoting[$u]=="nein"}active{/if}">Dagegen</button>
              				</div>
	              			<!--<a class="btn btn-success vote" voteName="{$u}" vote="ja">Dafür</a>
							<a class="btn btn-warning vote" voteName="{$u}" vote="enthaltung">Enthaltung</a>
							<a class="btn btn-danger vote" voteName="{$u}" vote="nein">Dagegen</a>!-->
						</td>
					</tr>
              {/foreach}
              </table>
              </div>
              {/if}
				<div class="tab-pane fade" id="log">
					<p>Hier siest du die Abstimmungs History:</p>
                <p>
					<table class="table">
						{foreach from=$log item=logItem}
								<tr><td>{$logItem["time"]|date_format:"%d.%m.%Y - %H:%M"}</td><td>{{$logItem["log"]}}</td></tr>
						{/foreach}
					</table>
				</p>
              </div>
              {if $berechtigung}
				<div class="tab-pane fade" id="admin">
					<p>Hier kannst du das Ergebniss festlegen und den Status ändern</p>
				<b>Ergebiss:</b><br>
				<div class="btn-group" data-toggle="buttons-radio">
              						<button type="button" voteName="{$u}" vote="angenommen" class="btn btn-success result {if $antrag["info"]=="angenommen"}active{/if}">Angenommen</button>
									<button type="button" voteName="{$u}" vote="abstimmung" class="btn btn-warning result {if $antrag["info"]=="abstimmung"}active{/if}">Abstimmung</button>
									<button type="button" voteName="{$u}" vote="abgelehnt" class="btn btn-danger result {if $antrag["info"]=="abgelehnt"}active{/if}">Abgelehnt</button>
              </div><br><br>
				<b>Status:</b><br>
				<select id="status">
					{foreach from=$status item=stati}
						<option {if $stati eq $antrag["status"]} selected {/if}>{{$stati}}</option>
					{/foreach}
				</select>
				<script language="javascript">
				$("#status").change( function() {
					window.location.href = "{{$pfad}}antrag/status/{{$id}}/"+$(this).val();
				});
				</script>
				{{if $antrag["gliederung"] == "Bund"}}
				<h3>Admin</h3>
				<a href="#" id="pushToWiki">Push To Wiki</a>
				{{/if}}
				</div>
				{/if}
            </div>

	</div>
<div class="span3">
<table class="table table-bordered">
<tr><td>ID</td><td>{{$antrag["antragsID"]}}</td></tr>
<tr><td>Gliederung</td><td>{{if $gliederungWiki["wikiSeite"]!=NULL}}<a href="https://wiki.junge-piraten.de/wiki/{{$gliederungWiki["wikiSeite"]}}">{/if}{{$antrag["gliederung"]}}{{if $gliederungWiki["wikiSeite"]!=NULL}}</a>{/if}</td></tr>
<tr><td>Budget</td><td>{{$antrag["budget"]}}</td></tr>
<tr><td>Status</td><td>{{$antrag["status"]}}</td></tr>
<tr><td>Ergebnis</td><td>{{$antrag["info"]}}</td></tr>
<tr><td>Autor</td><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:{{$antrag["antragssteller"]}}">{{$antrag["antragssteller"]}}</a></td></tr>
{if isset($antragsTime["gestellt"])}
<tr><td>Datum der Antragsstellung</td><td>{$antragsTime["gestellt"]|date_format:"%d.%m.%Y - %H:%M"}</td></tr>
{/if}
{if isset($antragsTime["beschluss"])}
<tr><td>Datum der Entscheidung</td><td>{$antragsTime["beschluss"]|date_format:"%d.%m.%Y - %H:%M"}</td></tr>
{/if}
</table>
</div>
	</div>
<script language="javascript">
function reloadBeschluss()
{
	$.ajax({
		type: "GET",
		url: "{{$pfad}}api/antrag/vote?id={{$antrag["id"]}}"
	}).done(function( msg ) {
		var obj = jQuery.parseJSON(msg);
		var text = '<p>So wurde abgestimmt von:</p><table class="table" style="width:50%;">';
		$.map(obj, function(value, key) {
			//text = text + "<tr><td>"+key+"</td><td>"+value+"</td></tr>";
			text = text + '<tr><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:'+key+'">'+key+'</a></td>';
			if(value=="ja")
			{
				text = text + '<td><a style="width:70px;cursor:text;" class="btn btn-success result active">Dafür</a></td></tr>';
			}
			if(value=="nein")
			{
				text = text + '<td><a style="width:70px;cursor:text;" class="btn btn-danger result active">Dagegen</a></td></tr>';
			}
			if(value=="enthaltung")
			{
				text = text + '<td><a style="width:70px;cursor:text;" class="btn btn-warning result active">Enthaltung</a></td></tr>';
			}
		});
		text = text + "</tabel>";
		$("#beschluss").html(text);
			//alert("Abgestimmt");
	});
	window.setTimeout("reloadBeschluss()", 5000);
}
</script>
{if $berechtigung == true}
<script lanugage="javascript">
$(".vote").click( function() {
	$.ajax({
	type: "PUT",
		url: "{{$pfad}}api/antrag/vote",
		data: { token: "{{$token}}", stimme: $(this).attr("vote"), antrag: {{$antrag["id"]}}, name: $(this).attr("voteName")  }
		}).done(function( msg ) {
			alert("Abstimmung erfolgreich");
			reloadBeschluss();
		});
});
$(".result").click( function() {
	$.ajax({
	type: "PUT",
		url: "{{$pfad}}api/antrag/result",
		data: { token: "{{$token}}", result: $(this).attr("vote"), antrag: {{$antrag["id"]}}  }
		}).done(function( msg ) {
			location.reload();
		});
});
$("#status").change( function() {
	$.ajax({
	type: "PUT",
		url: "{{$pfad}}api/antrag/status",
		data: { token: "{{$token}}", status: $(this).val(), antrag: {{$antrag["id"]}}  }
		}).done(function( msg ) {
			location.reload();
		});
});
$("#pushToWiki").click (function () {
	$.ajax({
	type: "PUT",
		url: "{{$pfad}}api/beta/pushToWiki",
		data: { antrag: {{$antrag["id"]}}  }
		}).done(function( msg ) {
			console.log(msg);
			alert("Wikiseite erstellt (beta)");
		});
});
</script>
{/if}
     
{include file='../template/bottom.tpl'}