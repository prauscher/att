<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Junge Piraten Anträge</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="{{$pfad}}static/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }

      ul.nav li.dropdown:hover > ul.dropdown-menu{
        display: block;    
      }
      a.menu:after, .dropdown-toggle:after {
        content: none;
      }


    </style>
    <link href="{{$pfad}}/static/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>
  <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{$pfad}}static/js/jquery.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-transition.js"  type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-alert.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-dropdown.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-scrollspy.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-tab.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-tooltip.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-popover.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-button.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-collapse.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-carousel.js" type="text/javascript"></script>
    <script src="{{$pfad}}static/js/bootstrap-typeahead.js" type="text/javascript"></script>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="{$pfad}">Antragstool</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="{{$pfad}}">Home</a></li>
              <!--<li><a href="{{$pfad}}antrag">Anträge</a></li>!-->
              <li class="dropdown">
    			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Anträge</a>
    			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
    				{foreach from=$gliederung item=g}
    					<li><a href="{{$pfad}}antrag/list/{$g}">{$g}</a></li>
    				{/foreach}
					
    			</ul>
    		  </li>
              <li><a href="{{$pfad}}antrag/neu">Neuen Antrag</a></li>
            </ul>
            {if isset($session["nutzerName"]) && $session["nutzerName"] !="gast"}
            {else}
            <!--
            <form class="navbar-form pull-right" method="POST" action="{{$pfad}}user/login">
              <input class="span2" type="text" placeholder="Username" name="user">
              <input class="span2" type="password" placeholder="Password" name="pw">
              <button type="submit" class="btn">Sign in</button>
            </form>
            !-->
            {/if}
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>