<?php
session_start();
//session_destroy();
if(!isset($_SESSION["nutzerName"]))
{
	$_SESSION["nutzerName"]="gast";
}
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../vendor/smarty/libs/Smarty.class.php';
require_once __DIR__.'/../config/server.config.php';

define("MYSQL_HOST", $mysql["host"]);
define("MYSQL_USER", $mysql["user"]);
define("MYSQL_PASS", $mysql["pass"]);
define("MYSQL_DB", $mysql["db"]);
define("NNTP_SERVER", $nntp["server"]);
define("NNTP_USER", $nntp["user"]);
define("NNTP_PASS", $nntp["pass"]);
define("path", $path["web"]);
define("pathOS", $path["os"]);
define("publickey", $capacha["public"]);
define("privatekey", $capacha["private"]);
define("superToken", "penispenis");

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Include General Models
if (is_dir("../models/")) {
	if ($dh = opendir("../models/")) {
		while (($file = readdir($dh)) !== false) {
			if(substr($file, 0, 1)!=".")
			{
				require_once "../models/".$file;
			}
		}
	}
}
//Include web Models
if (is_dir("./models/")) {
	if ($dh = opendir("./models/")) {
		while (($file = readdir($dh)) !== false) {
			if(substr($file, 0, 1)!=".")
			{
				require_once "./models/".$file;
			}
		}
	}
}

$app = new Silex\Application();
$app["debug"]=true;
$app->before(function (Request $request) {
	$token = $request->get("token", NULL);
	if($token!=NULL)
	{
		//$_SESSION["token"]=$token;
		tokenModel::setToken($token);
	}
	smartyModel::createSmarty();
	$detais = explode("/", str_replace($request->getBaseUrl(), "", $_SERVER['REQUEST_URI']));
	if(count($detais)==2)
	{
		$controller = "index";
		$view = $detais[1];
	}
	else
	{
		$controller = $detais[1];
		$view = $detais[2];
	}
	if($view=="")
	{
		$view="index";
	}
	smartyModel::$controller = $controller;
	smartyModel::$view = $view;
	$anstragsModel = new antragsModel();
	smartyModel::assign("gliederung", $anstragsModel->getGliederung());
});
$app->after(function (Request $request, Response $response) {
	
	//var_dump($detais);
	if(smartyModel::$render)
	{
		smartyModel::display(pathOS."web/views/".smartyModel::$controller."/".smartyModel::$view.".tpl");
	}
});
if (is_dir("./controller/")) {
	if ($dh = opendir("./controller/")) {
		while (($file = readdir($dh)) !== false) {
			if(substr($file, 0, 1)!=".")
			{
				$app->mount('/'.str_replace(array(".php", "index"), array("", ""),	$file), include 'controller/'.$file);
			}
		}
	}
}

$app->run();
