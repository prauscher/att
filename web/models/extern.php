<?php
class externModel extends DataBase
{
	public function createWikiPage($antragsID) //Only for Bund
	{
	    smartyModel::$render=false;
		$antragsModel = new antragsModel();
		//$Mediawiki = new Mediawiki();
		$antrag = $antragsModel->getById($antragsID);
		tokenModel::setAntrag($antragsID);
		$tokenModel = new tokenModel();
		$tokenModel->create();
		if($tokenModel->berechtigung())
		{
			if($antrag["gliederung"]!="Bund")
			{
				return false;
			}
			$voting = $antragsModel->getVoting($antragsID);
			$ergebniss = array("ja"=>0, "nein" => 0, "enthaltung"=>0);
			foreach($voting as $v)
			{
				$ergebniss[$v["voting"]]++;
				
			}
			$wiki = new Mediawiki("https://wiki.junge-piraten.de/w/api.php");
			$wiki->login("sspssp", "gelnhausen", "Junge Piraten");
			$page = $wiki->getPage("Vorstand/Beschluss/".$antrag["beschlussID"]." ".$antrag["name"]);
			
			//return false;
			$text = "{{Beschluss Seite\r\n";
			$text .= "|Organ = Bundesvorstand\r\n";
			$text .= "|Beschluss-Datum = ".$antrag["beschlussDatum"]."\r\n";
			//$text .= "|Beschluss-Nummer = ".$antrag["beschlussID"]."\r\n";
			//$text .= "|Beschluss-Titel = ".$antrag["name"]."\r\n";
			$text .= "|Beschluss-Text = ".$antrag["beschluss"]."\r\n";
			$text .= "|Antragssteller = ".$antrag["antragssteller"]."\r\n";
			$text .= "|ja-Stimmen = ".$ergebniss["ja"]."\r\n";
			$text .= "|nein-Stimmen = ".$ergebniss["nein"]."\r\n";
			$text .= "|Enthaltung-Stimmen = ".$ergebniss["enthaltung"]."\r\n";
			$text .= "}}";
			$page->setText($text);
			echo $text;
		}
		

	}
}
?>
