<?php
class logModel extends DataBase
{
	public function addLog($antragsID, $status, $aktion, $fuer = null)
	{
		$bevor = $this->getLastStatus($antragsID, $aktion, $fuer);
		if($status == $bevor)
		{
			return false;
		}
		$sql = "INSERT INTO `logDetails`(`id`, `antragsID`, `bevor`, `status`, `aktion`, `time`, `token`, `name`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";
		$this->insert($sql, array($antragsID, $bevor, $status, $aktion, time(), tokenModel::getToken(), $fuer));
		//OLD
		if($aktion=="gestellt")
		{
			$sql = "INSERT INTO `log`(`id`, `beschlussID`, `time`, `log`) VALUES (NULL, ?, ? ,?)";
			$this->insert($sql, array($antragsID, time(), "Antrag von ".$status." gestellt"));
		}

	}
	public function getLastStatus($antragsID, $aktion, $fuer)
	{
		$sql = "SELECT `status` FROM `logDetails` WHERE `antragsID` = ? AND `aktion` = ? AND `name` = ?";
		$res = $this->query($sql, array($antragsID, $aktion, $fuer));
		if(isset($res[0]["status"]))
		{
			return $res[0]["status"];
		}
		return false;
	}
	public function getLog($antragsID)
	{
		$sql = "SELECT * FROM `log` WHERE `beschlussID` = ?";
		$res = $this->query($sql, array($antragsID));
		$sql = "SELECT * FROM `logDetails` LEFT JOIN `token` ON `logDetails`.`token` = `token`.`token` WHERE `antragsID` = ?";
		$res2 = $this->query($sql, array($antragsID));
		foreach($res2 as $r)
		{
			if($r["aktion"]=="gestellt")
			{
				$log = "Antrag von ".$r["status"]." gestellt";
			}
			if($r["aktion"]=="abstimmen")
			{
				$log = $r["nutzer"]." stimmte im Namen von ".$r["name"];
				if($r["bevor"]!="")
				{
					$log .= " von ".$this->createStautsButton($r["bevor"]);
				}
				$log .= " auf ".$this->createStautsButton($r["status"]);
			}
			if($r["aktion"]=="ergebnis")
			{
				$log = "Ergebnis von ".$r["nutzer"]." auf ".$r["status"]." gestellt";
			}
			if($r["aktion"]=="status")
			{
				$log = "Status von ".$r["nutzer"]." auf ".$r["status"]." gestellt";
			}
			$res[] = array("id"=>0, "beschlussID"=>$r["antragsID"], "time"=>$r["time"], "log"=>$log);
		}
		return $res;
	}
	public function createStautsButton($status)
	{
		if($status=="enthaltung")
		{
			return '<span style="width:70px;cursor:text;" class="btn btn-warning result active">Enthaltung</span>';
		}
		if($status=="ja")
		{
			return '<span style="width:70px;cursor:text;" class="btn btn-success result active">Dafür</span>';
		}
		if($status=="nein")
		{
			return '<span style="width:70px;cursor:text;" class="btn btn-danger result active">Dagegen</span>';
		}
		if($status=="abstimmung")
		{
			return '<span style="width:70px;cursor:text;" class="btn btn-warning result active">Abstimmung</span>';
		}
		if($status=="angenommen")
		{
			return '<span style="width:70px;cursor:text;" class="btn btn-success result active">Angenommen</span>';
		}
		if($status=="abgelehnt")
		{
			return '<span style="width:70px;cursor:text;" class="btn btn-danger result active">Abgelehnt</span>';
		}
	}

}
?>