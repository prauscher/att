<?php
class userModel extends DataBase
{
	public function login($username, $pw)
	{
		$nntp = new nntpModel();
		$nntp->connect("news.junge-piraten.de");
		$login = $nntp->autentifizierung($username, $pw);
		if(!$login)
		{
			return false;
		}
		$_SESSION["nutzerName"]=$username;
	}
	public function getMailadress($nutzer)
	{
		$sql = "SELECT mail FROM Nutzer2Gliederung WHERE nutzer = ? AND mail != '' LIMIT 0, 1";
		$res = $this->query($sql, array($nutzer));
		if(!isset($res[0]["mail"]))
		{
			return $nutzer."@community.junge-piraten.de";
		}
		return $res[0]["mail"];

	}
	public function sendMail($nutzer, $betreff, $mail)
	{
		ini_set("SMTP", "mail.schredder.me");
		ini_set("sendmail_from", "soeren@siegismund-poschmann.de");
		ini_set("username" , "soeren@siegismund-poschmann.de");
		ini_set("password", "gelnhausen");
		ini_set("SMPT_PORT" , "25");

		mail($this->getMailadress($nutzer), $betreff, $mail." \r\nDies ist eine Automatische E-Mail, bitte nicht darauf Antworten!", "From:kontakt@junge-piraten.de\r\nReply-To:noreply@junge-piraten.de\r\nContent-type: text/html; charset=UTF-8");
	}
	public function generateMail($nutzer, $mailName, $data = array())
	{
		if($mailName=="newVoting")
		{
			$this->sendMail($nutzer, "Neue Abstimmung ".$data["antrag"]["name"], "Hallo, es gibt einen neuen Antrag\r\n\r\n".$data["antrag"]["beschluss"]."\r\n\r\nToken: ".$data["token"]."\r\n\r\n".path."antrag/antrag/".$data["id"]."?token=".$data["token"]);
		}
		if($mailName=="Reminder")
		{
			//echo "Hallo, es gibt einen Antrag für den noch kein Status oder kein Ergebniss gesetzt wurde. Bitte holt dieses nach.\r\n\r\n".$data["antrag"]["beschluss"]."\r\n\r\nToken: ".$data["token"]."\r\n\r\n".path."antrag/antrag/".$data["id"]."?token=".$data["token"];
			$this->sendMail($nutzer, "Reminder Abstimmung ".$data["antrag"]["name"], "Hallo, es gibt einen Antrag für den noch kein Status oder kein ERgebniss gesetzt wurde. Bitte holt dieses nach.\r\n\r\n".$data["antrag"]["beschluss"]."\r\n\r\nToken: ".$data["token"]."\r\n\r\n".path."antrag/antrag/".$data["id"]."?token=".$data["token"]);
		}
	}
}
?>
