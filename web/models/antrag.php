<?php
class antragsModel extends DataBase
{
	public function neu ($name, $antrag, $gliederung, $begruendung = "", $budget = 0, $steller = "", $stellerMail = NULL)
	{
		$sql = "SELECT COUNT(id) FROM `antrag` WHERE `erstellDatum` BETWEEN ? AND ?";
		$res = $this->query($sql, array(date("Y-m-d")." 00:00:00", date("Y-m-d")." 23:59:59"));
		$antragsIDN = 0;
		if(isset($res[0][0]))
			$antragsIDN = $res[0][0];
		$antragsIDN++;
		$antragsID = date("Ymd");
		$antragsID .= sprintf("%'03d",$antragsIDN);
		$sql = 'INSERT INTO `antrag`(`id`, `name`, `beschluss`, `begruendung`, `budget`, `gliederung`, `antragssteller`, `antragsstellerMail`, `old`, `antragsID`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, 0, ?)';
		$id = $this->insertID($sql, array($name, $antrag, $begruendung, $budget, $gliederung, $steller, $stellerMail, $antragsID));
		$logModel = new logModel();
		$logModel->addLog($id, $steller, "gestellt", "");
		return $id;
	}
	public function announceAntrag($antragsID)
	{
	  $antrag = $this->getById($antragsID);
	  //echo "ANTRAG";
	  //var_dump($antrag);
	  //echo "Gliederng";
	  $gliederung = $this->getGliederungDetails($antrag["gliederung"]);
	  //var_dump($gliederung);exit();
	  if(!empty($gliederung["nnptName"]))
	  {
	    //var_dump($nntpModel = new nntpModel());
	    //var_dump($nntpModel->connect(NNTP_SERVER));
	    //var_dump($nntpModel->autentifizierung(NNTP_USER, NNTP_PASS));
	    //exit();
	    $nntpModel = new nntpModel();
	    $nntpModel->connect(NNTP_SERVER);
	    $nntpModel->autentifizierung(NNTP_USER, NNTP_PASS);
	    $nntpModel->post($gliederung["nnptName"], "Neuer Antrag ".$antrag["name"], "Hallo,\r\nfolgender Antrag wurde gestellt: \r\n\r\n".$antrag["beschluss"]."\r\n\r\nBegründung:\r\n\r\n".$antrag["begruendung"]."\r\n\r\nDer Antrag kann unter ".path."antrag/antrag/".$antragsID." eingesehen werden", "bot@junge-piraten.de", array("Message-ID" => "<".md5($antragsID)."@jupisantrag.de>"));
	    return true;
	  }
	  //exit();
	  
	}
	public function createTokens($antragsID)
	{
		$antrag = $this->getById($antragsID);
		$berechtigte = $this->getAbstimmungsBerechtigte($antrag["gliederung"]);
		foreach($berechtigte as $name)
		{
			$erfolg = false;
			$i = 0;
			$userModel = new userModel();
			while($erfolg == false && $i < 5)
			{
				$i++;
				$token = $this->createToken($name);
				$sql = "SELECT count(`token`) FROM `token` WHERE `token` = ?";
				$res = $this->query($sql, array($token));
				if($res[0][0]==0)
				{
					$sql = "INSERT INTO `token`(`token`, `nutzer`, `antrag`) VALUES (?,?,?)";
					$this->insert($sql, array($token, $name, $antragsID));
					$erfolg = true;
					$userModel->generateMail($name, "newVoting", array("token"=>$token, "id"=>$antragsID, "antrag"=>$antrag));
				}
			}
		}
	}
	public function checkToken($token)
	{
		$sql = "SELECT * FROM `token` WHERE `token` = ?";
		$res = $this->query($sql, array($token));
		if(count($res)==1)
		{
			return $res[0];
		}
		return false;
	}
	private function createToken($name)
	{
		$token = date("d.m.Y").rand(100,999).date("H:m:s").rand(100,999).$name;
		return substr(hash("sha512", $token), rand(0, 50), rand(25,35));
	}
	public function getToken($name, $antragsID)
	{
		$sql = "SELECT `token` FROM `token` WHERE `nutzer` = ? AND `antrag` =?";
		$res = $this->query($sql, array($name, $antragsID));
		if(isset($res[0]["token"]))
		{
			return $res[0]["token"];
		}
		return false;
	}
	public function getTokens($antragsID)
	{
		$sql = "SELECT `token`, `nutzer` FROM `token` WHERE `antrag` =?";
		$res = $this->query($sql, array($antragsID));
		return $res;
	
	}
	public function getById($id)
	{
		$sql = 'SELECT * FROM `antrag` WHERE id = ?';
		$res = $this->query($sql, array($id));
		if(isset($res[0]))
		{
			return $res[0];
		}
		return false;
	}
	public function berechtigt($nutzerName, $gliederung, $antragsID = null)
	{
		$sql = "SELECT count(nutzer) FROM `Nutzer2Gliederung` WHERE nutzer = ? AND `gliederung` = ?";
		$res = $this->query($sql, array($nutzerName, $gliederung));
		if($res[0][0]==1)
		{	
			/*if($antragsID!=null)
			{
				$antrag = $this->getById($antragsID);
				if(!($antrag["status"]=="eingereicht"||$antrag["status"]=="voting"))
				{
					return false;
				}
				
			}*/
			return true;
		}
		return false;
	}
	public function getStatus()
	{
		$sql = "SELECT * FROM status";
		$res = $this->query($sql, array());
		$statuslist = array();
		foreach($res as $stati)
		{
			$statuslist[] = $stati[0];
		}
		return $statuslist;
	}
	public function abstimmung($antragID, $nutzerName, $stimme, $votingUser = NULL)
	{
		if($votingUser==NULL)
		{
			$votingUser = $nutzerName;
		}
		$antrag = $this->getById($antragID);
		if($this->berechtigt($votingUser, $antrag["gliederung"], $antragID))
		{
			$this->setVote($antragID, $nutzerName, $stimme, $votingUser);
			/*$sql = "INSERT INTO `voting` (`beschlussID`, `nutzerName`, `voting`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE voting = ?";
			$this->insert($sql, array($antragID, $nutzerName, $stimme, $stimme));
			$logModel = new logModel();
			$logModel->addLog($antragID, $stimme, "abstimmen", $nutzerName);
			//$sql = "INSERT INTO `log`(`id`, `beschlussID`, `time`, `log`) VALUES (NULL, ?, ? ,?)";
			//$this->insert($sql, array($antragID, time(), $votingUser." stimmte im Namen von ".$nutzerName." mit ".$stimme));
			*/
		}
	}
	public function setVote($antragID, $nutzerName, $stimme, $votingUser)
	{
		$sql = "INSERT INTO `voting` (`beschlussID`, `nutzerName`, `voting`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE voting = ?";
		$this->insert($sql, array($antragID, $nutzerName, $stimme, $stimme));
		$logModel = new logModel();
		$logModel->addLog($antragID, $stimme, "abstimmen", $nutzerName);
	}
	public function getVoting($antragsID)
	{
		$sql = "SELECT * FROM `voting` WHERE `beschlussID` = ?";
		$res = $this->query($sql, array($antragsID));
		return $res;
	}
	public function getVote($antragsID)
	{
		$sql = "SELECT * FROM `voting` WHERE `beschlussID` = ?";
		$res = $this->query($sql, array($antragsID));
		$vote = array();
		foreach($res as $row)
		{
			$vote[$row["nutzerName"]]=$row["voting"];
		}
		return $vote;
	}
	public function getLog($antragsID)
	{
		$logModel = new logModel();
		return $logModel->getLog($antragsID);
	}
	public function setErgebnis($antragsID, $ergebniss, $nutzername = NULL)
	{
		if($nutzername==NULL)
		{
			$nutzername = $_SESSION["nutzerName"];
		}
		$antrag = $this->getById($antragsID);
		if($this->berechtigt($nutzername, $antrag["gliederung"]))
		{
			$sql = "SELECT beschlussID FROM antrag WHERE id = ?";
			$res = $this->query($sql, array($antragsID));
			if(strlen($res[0][0])==NULL)
			{
				$sql = "SELECT COUNT(id) FROM `antrag` WHERE (gliederung = ?) AND (`beschlussDatum` BETWEEN ? AND ?)";
				$res = $this->query($sql, array($antrag["gliederung"], date("Y-m-d")." 00:00:00", date("Y-m-d")." 23:59:59"));
				$beschlussIDN = $res[0][0];
				$beschlussIDN++;
				$beschlussID = date("Ymd");
				$beschlussID .= sprintf("%'03d",$beschlussIDN);
				$sql = "UPDATE `antrag` SET `beschlussID`= ?, beschlussDatum = CURRENT_TIMESTAMP WHERE id = ?";
				$this->insert($sql, array($beschlussID, $antragsID));
				
			}
			$sql = "UPDATE `antrag` SET `info`= ?  WHERE id = ?";
			$res = $this->insert($sql, array($ergebniss, $antragsID));
			$logModel = new logModel();
			$logModel->addLog($antragsID, $ergebniss, "ergebnis");
			//$sql = "INSERT INTO `log`(`id`, `beschlussID`, `time`, `log`) VALUES (NULL, ?, ? ,?)";
			//$this->insert($sql, array($antragsID, time(), $nutzername." setzt das Ergebniss auf ".$ergebniss));
			if($antrag["antragsstellerMail"]!=NULL)
			{
				mail($antrag["antragsstellerMail"], "Dein Antrag: ".$antrag["name"]." hat das Ergebniss ".$ergebniss, "Ohai, dein Antrag wurde behandelt.", "From:kontakt@junge-piraten.de");
			}
			return true;
		}
		return false;
	}
	public function setStatus($antragsID, $status)
	{
		$antrag = $this->getById($antragID);
		$tokenModel = new tokenModel();
		$tokenModel->setAntrag($antragsID);
		$tokenModel->setTokenByUser($antragsID);
		//var_dump($tokenModel->berechtigung());
		if($tokenModel->berechtigung())
		{
			$sql = "UPDATE `antrag` SET `status`= ? WHERE id = ?";
			$res = $this->query($sql, array($status, $antragsID));
			$logModel = new logModel();
			$logModel->addLog($antragsID, $status, "status");
			//$sql = "INSERT INTO `log`(`id`, `beschlussID`, `time`, `log`) VALUES (NULL, ?, ? ,?)";
			//$this->insert($sql, array($antragsID, time(), $_SESSION["nutzerName"]." setzt den Status auf ".$status));
			return true;
		}
		return false;

	}
	public function listAntrage($antragsFilter)
	{
		$sqldata = array();
		$sql = "SELECT * FROM antrag ";
		if($antragsFilter->getGliederung()!=null)
		{
			$sql .= "WHERE gliederung = ? ";
			$sqldata[] = $antragsFilter->getGliederung();
		}
		else
		{
			$sql .= "WHERE gliederung != 'Test' ";
		}
		//status
		if($antragsFilter->getAutor()!=null)
		{
			$sql .= "AND antragssteller = ? ";
			$sqldata[] = $antragsFilter->getAutor();
		}
		if($antragsFilter->getStatus()!=null)
		{
			$sql .= "AND status = ? ";
			$sqldata[] = $antragsFilter->getStatus();
		}
		$sql .= "ORDER BY `antrag`.`id` DESC ";
		$sql .= "LIMIT ".($antragsFilter->getPage()*$antragsFilter->getPageLimit()).", ".($antragsFilter->getPage()*$antragsFilter->getPageLimit()+$antragsFilter->getPageLimit());

		//echo $sql;
		return $this->query($sql, $sqldata);
	}
	public function getGliederung($name = null)
	{
		$sql = "SELECT * FROM gliederung";
		if($name!=null)
		{
		  $sql.= ' WHERE name = "'.$name.'"';
		}
		$res = $this->query($sql, array());
		$gliederung = "";
		foreach($res as $row)
		{
			$gliederung[] = $row[0];
		}
		return $gliederung;
	}
	public function getGliederungDetails($name = null)
	{
	  $sql = "SELECT * FROM gliederung";
	  if($name!=null)
	  {
	    $sql.= ' WHERE name = "'.$name.'"';
	  }
	  $res = $this->query($sql, array());
	  if($name!=null)
	  {
	    return $res[0];
	  }
	  return $res;
	}
	public function getAbstimmungsBerechtigte($gliederung)
	{
		$sql = "SELECT * FROM `Nutzer2Gliederung` WHERE `gliederung` = ?";
		$res = $this->query($sql, array($gliederung));
		$name = array();
		foreach($res as $row)
		{
			$name[] = $row[0];
		}
		return $name;
	}
	public function getAntragsDaten($antragsID)
	{
		$time = array();
		$sql = "SELECT time  FROM `logDetails` WHERE `antragsID` = ? AND `aktion` = 'gestellt'";
		$res = $this->query($sql, array($antragsID));
		if(isset($res[0]["time"]))
		{
			$time["gestellt"] = $res[0]["time"];
		}
		$sql = "SELECT time  FROM `logDetails` WHERE `antragsID` = ? AND `aktion` = 'ergebnis' ORDER BY `logDetails`.`time`  DESC LIMIT 0, 1";
		$res = $this->query($sql, array($antragsID));
		if(isset($res[0]["time"]))
		{
			$time["beschluss"] = $res[0]["time"];
		}
		return $time;
	}
	public function getOffeneAntraege()
	{
		$sql = 'SELECT * FROM `antrag` WHERE NOW()  > ADDTIME(`erstellDatum`, "5 1:1:1.000002") AND (`status` = "eingereicht" OR `info` = "abstimmung") AND `gliederung` != "Test"';
		$res = $this->query($sql, array());
		return $res;
	}
	public function sendReminders()
	{
		$res = $this->getOffeneAntraege();
		$userModel = new userModel();
		foreach($res as $row)
		{
			$tokens = $this->getTokens($row["id"]);
			foreach($tokens as $token)
			{
				$userModel->generateMail($token["nutzer"], "Reminder", array("token"=>$token["token"], "id"=>$row["id"], "antrag"=>$row));
			}
		}
	}
	//Das ist eine seriöse Methode für den Vorstandsbot
	public function getNamen($name)
	{
		$sql = 'SELECT nutzer FROM `Nutzer2Gliederung` WHERE (nutzer = ? OR `namenMapping` = ?) AND `gliederung` = "Bund"';
		$res = $this->query($sql, array(trim($name), trim($name)));
		if(count($res)==1)
		{
			return $res[0]["nutzer"];
		}
		return false;
	}
}
class AntragsFilter
{
	private $gliederung = null;
	private $page = 0;
	private $pageLimit = 20;
	private $autor = null;
	private $status = null;
	public function setGliederung($gliederung)
	{
		$this->gliederung = $gliederung;
	}
	public function getGliederung()
	{
		return $this->gliederung;
	}
	public function setPage($page)
	{
		$this->page = $page;
	}
	public function getPage()
	{
		return $this->page;
	}
	public function setPageLimit($pageLimit)
	{
		$this->pageLimit = $pageLimit;
	}
	public function getPageLimit()
	{
		return $this->pageLimit;
	}
	public function setAutor($autor)
	{
		$this->autor = $autor;
	}
	public function getAutor()
	{
		return $this->autor;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}
	public function getStatus()
	{
		return $this->status;
	}

	
}
?>
