<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$search = $app['controllers_factory'];

$search->get("/", function () {
	return " ";
});

$search->get("/unbearbeitet", function(Request $request) {
	smartyModel::$controller="antrag";
	smartyModel::$view="index";
	$antragsModel = new antragsModel();
	smartyModel::assign("antrage", $antragsModel->getOffeneAntraege());
	return " ";
});

$search->get("/status/{status}", function($status, Request $request) {
	smartyModel::$controller="antrag";
	smartyModel::$view="index";
	$antragsModel = new antragsModel();
	$AntragsFilter = new AntragsFilter();
	$AntragsFilter->setStatus($status);
	smartyModel::assign("antrage", $antragsModel->listAntrage($AntragsFilter));
	return " ";
});

/*
$search->get("/text/{text}", function($text, Request $request) {
	smartyModel::$controller="antrag";
	smartyModel::$view="index";
	$antragsModel = new antragsModel();
	$AntragsFilter = new AntragsFilter();
	$AntragsFilter->setStatus($status);
	smartyModel::assign("antrage", $antragsModel->listAntrage($AntragsFilter));
	//var_dump($antragsModel->listAntrage());
	return " ";
});
*/

return $search;
?>