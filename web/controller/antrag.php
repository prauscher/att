<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$index = $app['controllers_factory'];

$index->get("/list/{gliederung}", function ($gliederung) {
	$antragsModel = new antragsModel();
	$AntragsFilter = new AntragsFilter();
	$AntragsFilter->setGliederung($gliederung);
	smartyModel::assign("antrage", $antragsModel->listAntrage($AntragsFilter));
	smartyModel::$view="index";
	//var_dump($antragsModel->listAntrage());
	return " ";
});

$index->get("/neu", function () {
	$anstragsModel = new antragsModel();
	smartyModel::assign("spam", recaptcha_get_html(publickey), true);
	return " ";
});
/*
//Hack
$index->get("/pushToWiki/{id}", function ($id) use ($app) {
	smartyModel::$render = false;
	$externModel = new externModel();
	var_dump($externModel->createWikiPage($id));
	//return $app->redirect(path."antrag/antrag/".$id);
	return " ";
});
$index->put("/pushToWiki/{id}", function ($id) use ($app) {
	smartyModel::$render = false;
	$externModel = new externModel();
	var_dump($externModel->createWikiPage($id));
	//return $app->redirect(path."antrag/antrag/".$id);
	return " ";
});
*/
/*$index->post("/neu", function (Request $reqeust) use ($app)
{

});
*/

$index->post("/neu", function (Request $request) use ($app) {
	smartyModel::$render = false;
	$url = path.'api/antrag';
	$url = "https://antraege.junge-piraten.de/api/antrag";
	$fields = array(
			'name' => urlencode($request->get('titel')),
			'text' => urlencode($request->get('text')),
			'gliederung' => urlencode($request->get('gliederung')),
			'begruendung' => urlencode($request->get('begruendung')),
			'budget' => urlencode($request->get('budget')),
			'autor' => urlencode($request->get('autor')),
			'autorMail' => urlencode($request->get("autorMail"))
	);
	$fields_string = "";
	foreach($fields as $key=>$value) {
		$fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	//var_dump($result);
	$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	var_dump($http_status);
	if($http_status==200)
	{
		$return = json_decode($result);
		if(isset($return->id))
		{
			return $app->redirect(path."antrag/antrag/".$return->id);
		}
		var_dump($return);
	}
	else
	{
		var_dump($result);
	}
	return " ";
});
$index->get("/antrag/{id}", function ($id) {
	smartyModel::assign("spam", recaptcha_get_html(publickey));
	$antragsModel = new antragsModel();
	$antrag = $antragsModel->getById($id);
	$gliederung = $antragsModel->getGliederungDetails($antrag["gliederung"]);
	smartyModel::assign("boardID", $gliederung["boardID"]);
	smartyModel::assign("threadid", urlencode(base64_encode("<".md5($id)."@jupisantrag.de>")));
	smartyModel::assign("antrag", $antrag);
	smartyModel::assign("id", $id);
	smartyModel::$view = "antrag";
	tokenModel::setAntrag($id);
	$tokenModel = new tokenModel();
	$tokenModel->setTokenByUser();
	$berechtigung = $tokenModel->berechtigung();
	smartyModel::assign("token", $tokenModel->getToken());
	smartyModel::assign("berechtigung", $berechtigung);
	smartyModel::assign("nutzerName", $tokenModel->getNutzerName());
	/*if(isset($_SESSION["token"]))
	{
		$url = path.'api/token/rights';
		$fields = array(
				'token' => urlencode($_SESSION["token"]),
				'antrag' => urlencode($id),
		);
		$fields_string = "";
		foreach($fields as $key=>$value) {
			$fields_string .= $key.'='.$value.'&';
		}
		rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($http_status==200)
		{
			smartyModel::assign("berechtigung", true);
		}
	}*/
	smartyModel::assign("voting", $antragsModel->getVoting($id));
	smartyModel::assign("log", $antragsModel->getLog($id), true);
	smartyModel::assign("status", $antragsModel->getStatus());
	smartyModel::assign("gliederungWiki", $antragsModel->getGliederungDetails($antrag["gliederung"]));
	smartyModel::assign("user", $antragsModel->getAbstimmungsBerechtigte($antrag["gliederung"]));
	smartyModel::assign("personVoting", $antragsModel->getVote($id));
	smartyModel::assign("antragsTime", $antragsModel->getAntragsDaten($id));
	return " ";
});
/*
$index->get("/antrag/{id}/{stimme}", function ($id, $stimme) use ($app) {
	$antragsModel = new antragsModel();
	$antragsModel->abstimmung($id, $_SESSION["nutzerName"], $stimme);
	smartyModel::$controller="index";
	smartyModel::$view = "empty";
	return $app->redirect(path."antrag/antrag/".$id);
});
*/
/*
$index->get("/antrag/{id}/{stimme}/{name}", function ($id, $stimme, $name) use ($app) {
	$antragsModel = new antragsModel();
	$antragsModel->abstimmung($id, $name, $stimme);
	smartyModel::$controller="index";
	smartyModel::$view = "empty";
	return $app->redirect(path."antrag/antrag/".$id);
});
*/
$index->get("/status/{id}/{stimme}", function ($id, $stimme) use ($app) {
	$antragsModel = new antragsModel();
	$antragsModel->setStatus($id, $stimme);
	smartyModel::$controller="index";
	smartyModel::$view = "empty";
	//return " ";
	return $app->redirect(path."antrag/antrag/".$id);
});
$index->get("/ergebniss/{id}/{stimme}", function ($id, $stimme) use ($app) {
	$antragsModel = new antragsModel();
	$antragsModel->setErgebnis($id, $stimme);
	smartyModel::$controller="index";
	smartyModel::$view = "empty";
	return $app->redirect(path."antrag/antrag/".$id);
});

return $index;
