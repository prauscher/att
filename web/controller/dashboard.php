<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$index = $app['controllers_factory'];

$index->get("/neu", function () {
	return " ";
});

$index->post("/login", function (Request $request) use ($app) {
	$userModel = new userModel();
	$login = $userModel->login($request->get('user'), $request->get('pw'));
	//var_dump($login);
	if($login==NULL)
	{
		return $app->redirect(path."dashboard/");
	}
	smartyModel::assign("login", $login);
	return "";
	//return $app->redirect(path."login?key=".$login->getKey());
	
});

return $index;