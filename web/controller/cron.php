<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$cron = $app['controllers_factory'];

$cron->get("/", function () {
	return " ";
});

$cron->get("/antrage", function(Request $request) {
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$antragsModel->sendReminders();
	return " ";
});
return $cron;
?>