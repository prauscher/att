<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$index = $app['controllers_factory'];

$index->get("/", function () use ($app) {
	//return $app->redirect(path."antrag/list/Bund");
	$antragsModel = new antragsModel();
	$AntragsFilter = new AntragsFilter();
	$AntragsFilter->setPageLimit(5);
	smartyModel::assign("antrage", $antragsModel->listAntrage($AntragsFilter));
	return " ";
});

$index->get("/test", function () {
	return "TEST";
});

return $index;