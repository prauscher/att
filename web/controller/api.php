<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$api = $app['controllers_factory'];

$api->get("/", function () {
	return " ";
});

$api->get("antrage", function(Request $request) {
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$antragsFilter = new AntragsFilter();
	$antragsFilter->setGliederung($request->get('gliederung'));
	$list = $antragsModel->listAntrage($antragsFilter);
	$res["status"]="success";
	$res["code"]=200;
	$res["retData"]=$list;
	return new Response(json_encode($res), 200);
	//return json_encode($res);
});
$api->post("antrag", function(Request $request) {
	$res = array();
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$name = $request->get('name');
	$antrag = $request->get('text');
	$gliederung = $request->get('gliederung');
	$begruendung = $request->get('begruendung');
	$budget = $request->get('budget');
	$steller = $request->get('autor');
	$stellerMail = $request->get('autorMail');
	if($name=="")
	{
		$res["status"]="missingParameter";
		$res["details"]="Name is Empty";
		return new Response(json_encode($res), 400);
	}
	if($antrag=="")
	{
		$res["status"]="missingParameter";
		$res["details"]="Antrag is Empty";
		return new Response(json_encode($res), 400);
	}
	$id = $antragsModel->neu($name, $antrag, $gliederung, $begruendung, $budget, $steller , $stellerMail);
	$antragsModel->announceAntrag($id);
	$antragsModel->createTokens($id);
	if($id>0)
	{
		return new Response(json_encode(array("id"=>$id)), 200);
	} else {
		return new Response("", 500);
	}
	return json_encode($res);
	
	
});
$api->get("antrag", function(Request $request) {
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$antrag = $antragsModel->getById($request->get('id'));
	return new Response(json_encode($antrag), 200);
});
$api->put('antrag/vote', function (Request $request) {
	
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$tokenDetails = $antragsModel->checkToken($request->get('token'));
	$name = $request->get('name', $tokenDetails["nutzer"]);
	if($name=="")
	{
		$name = $tokenDetails["nutzer"];
	}
	if(!$tokenDetails)
	{
		return new Response("Wrong Token", 401);
	}
	if($tokenDetails["antrag"]!=$request->get("antrag"))
	{
		return new Response("Wrong Token/Antrag", 401);
	}
	$antrag = $antragsModel->getById($tokenDetails["antrag"]);
	if(!$antragsModel->berechtigt($tokenDetails["nutzer"], $antrag["gliederung"]))
	{
		return new Response("No Rights to Vote", 401);
	}
	if(!$antragsModel->berechtigt($tokenDetails["nutzer"], $antrag["gliederung"], $tokenDetails["antrag"]))
	{
		return new Response("No Rights to Vote on this Voting", 403);		
	}
	if(!$antragsModel->berechtigt($name, $antrag["gliederung"], $tokenDetails["antrag"]))
	{
		return new Response("No rights of Voting user", 401);
	}
	$antragsModel->abstimmung($tokenDetails["antrag"], $name, $request->get('stimme'), $tokenDetails["nutzer"]);
	return new Response("", 200);
	return "JA";
});
$api->put('antrag/result', function (Request $request) {
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$antrag = $antragsModel->getById($request->get('antrag'));
	$tokenDetails = $antragsModel->checkToken($request->get('token'));
	if(!$tokenDetails)
	{
		return new Response("Wrong Token", 401);
	}
	if($tokenDetails["antrag"]!=$request->get("antrag"))
	{
		return new Response("Wrong Token/Antrag", 401);
	}
	$antrag = $antragsModel->getById($tokenDetails["antrag"]);
	if(!$antragsModel->berechtigt($tokenDetails["nutzer"], $antrag["gliederung"]))
	{
		return new Response("No Rights to Vote", 401);
	}
	$antragsModel->setErgebnis($request->get('antrag'), $request->get("result"), $tokenDetails["nutzer"]);
	return new Response("", 200);
});
$api->put('antrag/status', function (Request $request)
{
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$antrag = $antragsModel->getById($request->get('antrag'));
	$tokenDetails = $antragsModel->checkToken($request->get('token'));
	if(!$tokenDetails)
	{
		return new Response("Wrong Token", 401);
	}
	if($tokenDetails["antrag"]!=$request->get("antrag"))
	{
		return new Response("Wrong Token/Antrag", 401);
	}
	$antrag = $antragsModel->getById($tokenDetails["antrag"]);
	if(!$antragsModel->berechtigt($tokenDetails["nutzer"], $antrag["gliederung"]))
	{
		return new Response("No Rights to Vote", 401);
	}
	//$antragsModel->setErgebnis($request->get('antrag'), $request->get("status"), $tokenDetails["nutzer"]);
	if($antragsModel->setStatus($request->get('antrag'), $request->get("status")))
	{
		return new Response("", 200);
	}
	else
	{
		return new Response("Error", 401);
	}
});
$api->get('antrag/vote', function (Request $request)
	{
		smartyModel::$render=false;
		$antragsModel = new antragsModel();
		$voting = $antragsModel->getVote($request->get("id"));
		return new Response(json_encode($voting), 200);
	});
$api->get('token/rights', function () {
	smartyModel::$render=false;
	$antragsModel = new antragsModel();
	$tokenDetails = $antragsModel->checkToken($request->get('token'));
	if(!$tokenDetails)
	{
		return new Response("Wrong Token", 401);
	}
	if($tokenDetails["antrag"]!=$request->get("antrag"))
	{
		return new Response("Wrong Token/Antrag", 401);
	}
	$antrag = $antragsModel->getById($tokenDetails["antrag"]);
	if(!$antragsModel->berechtigt($tokenDetails["nutzer"], $antrag["gliederung"]))
	{
		return new Response("No Rights to Vote", 401);
	}
	return new Response("", 200);
});

//Not finish develop functions
$api->put('beta/pushToWiki', function(Request $request) 
{
	smartyModel::$render=false;
	$externModel = new externModel();
	$externModel->createWikiPage($request->get("antrag"));
	return new Response("", 202);
});
$api->get('beta/pushToWiki', function(Request $request)
{
  smartyModel::$render=false;
  $externModel = new externModel();
  $externModel->createWikiPage($request->get("antrag"));
  return new Response("", 202);
});

$api->get('beta/pushToForum', function(Request $request)
{
  smartyModel::$render=false;
  $antragsModel = new antragsModel();
  $status = $antragsModel->announceAntrag($request->get("antrag"));
  var_dump($status);
  return new Response(" ", 202);
});


$api->get('vorstandsbot/add', function(Request $request)
{
	smartyModel::$render=false;
	$res = array();
	if(superToken!==$request->get('superToken'))
	{
		return new Response(json_encode($res), 500);
	}
	$antragsModel = new antragsModel();
	$name = $request->get('name');
	$antrag = $request->get('text');
	$gliederung = "Bund";
	$begruendung = $request->get('begruendung');
	$budget = $request->get('budget');
	$steller = $request->get('autor');
	$stellerMail = NULL;
	if($name=="")
	{
		$res["status"]="missingParameter";
		$res["details"]="Name is Empty";
		return new Response(json_encode($res), 400);
	}
	if($antrag=="")
	{
		$res["status"]="missingParameter";
		$res["details"]="Antrag is Empty";
		return new Response(json_encode($res), 400);
	}
	$id = $antragsModel->neu($name, $antrag, $gliederung, $begruendung, $budget, $steller , $stellerMail);
	$dafuer = $request->get('dafuer');
	$dagegen = $request->get('dagegen');
	$enthalung = $request->get('enthaltung');
	$dafuer = explode(",", $dafuer);
	$dagegen = explode(",", $dagegen);
	$enthaltung = explode(",", $enthalung);
	foreach($dafuer as $name)
	{
		$name = $antragsModel->getNamen($name);
		if($name!=false)
		{
			$antragsModel->setVote($id, $name, "ja", $name);
		}
	}
	foreach($dagegen as $name)
	{
		$name = $antragsModel->getNamen($name);
		if($name!=false)
		{
			$antragsModel->setVote($id, $name, "nein", $name);
		}
	}
	foreach($enthaltung as $name)
	{
		$name = $antragsModel->getNamen($name);
		if($name!=false)
		{
			$antragsModel->setVote($id, $name, "enthaltung", $name);
		}
	}

	//$antragsModel->announceAntrag($id);
	//$antragsModel->createTokens($id);
	if($id>0)
	{
		return new Response(json_encode(array("id"=>$id)), 200);
	} else {
		return new Response("", 500);
	}
	return json_encode($res);
});
    



return $api;
?>