<?php /* Smarty version Smarty-3.1.13, created on 2013-04-09 13:47:40
         compiled from "/var/www/att/web/views/antrag/neu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:879096795160215ea4a5d8-04427224%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'da7a4989cf12c0b2706d4fcae1de6cc851fca714' => 
    array (
      0 => '/var/www/att/web/views/antrag/neu.tpl',
      1 => 1365508056,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '879096795160215ea4a5d8-04427224',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5160215eaa6412_70651750',
  'variables' => 
  array (
    'gliederung' => 0,
    'glied' => 0,
    'spam' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5160215eaa6412_70651750')) {function content_5160215eaa6412_70651750($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('../template/top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



    <div class="container">

      <!-- Example row of columns -->
      <div class="row">
        <div class="span12">
          <h2>Neuer Antrag</h2>
          Hier kannst du einen neuen Antrag an eine Gliederung der Jupis stellen.<hr>
        </div>
      </div>
      <div class="row">
        <div class="span12">
              <form class="form-horizontal" method="post">
    <div class="control-group">
    <label class="control-label" for="inputEmail">Antragsname</label>
    <div class="controls">
    <input type="text" style="width:400px;"  name="titel" value="">
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputPassword">Antragstext</label>
    <div class="controls">
    <textarea rows="3" style="width:400px;" name="text"></textarea>
    </div>
    </div>
   
    
    <div class="control-group">
    <label class="control-label" for="inputPassword">Antragsbegründung*</label>
    <div class="controls">
    <textarea rows="3" style="width:400px;" name="begruendung"></textarea>
    </div>
    </div>
    
        <div class="control-group">
    <label class="control-label" for="inputPassword">Budget* </label>
    <div class="controls">
        <div class="input-prepend input-append">
    <span class="add-on">EURO</span>
    <input class="span2" id="appendedPrependedInput" type="text" name="budget">
    <span class="add-on">.00</span>
    </div>
    </div>
    </div>
    
     <div class="control-group">
    <label class="control-label" for="inputPassword">Gliederung</label>
    <div class="controls">
    <select style="width:400px;" name="gliederung">
    <option>Bund</option>
    <?php  $_smarty_tpl->tpl_vars['glied'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['glied']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['gliederung']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['glied']->key => $_smarty_tpl->tpl_vars['glied']->value){
$_smarty_tpl->tpl_vars['glied']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['glied']->value!="Bund"){?> 
    	   <option><?php echo $_smarty_tpl->tpl_vars['glied']->value;?>
</option>
        <?php }?>
    <?php } ?>
    </select>
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputEmail">Antragssteller*</label>
    <div class="controls">
    <input type="text" id="user" style="width:400px; placeholder="Username" name="autor">
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputEmail">Mailadresse*</label>
    <div class="controls">
    <input type="text" id="user" style="width:400px; placeholder="Username" name="autorMail">
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="inputEmail">Spam</label>
    <div class="controls">
    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['spam']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>

    </div>
    </div>
    
 <div class="control-group">
<div class="controls">
<button type="submit" class="btn">Stellen</button>
<br><br>* Muss <b>nicht</b> ausgefüllt werden
</div>
</div>
    </form>
        </div>
      </div> 

     
<?php echo $_smarty_tpl->getSubTemplate ('../template/bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>