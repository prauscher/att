<?php /* Smarty version Smarty-3.1.13, created on 2013-04-06 15:53:14
         compiled from "/var/www/att/web/views/index/api.tpl" */ ?>
<?php /*%%SmartyHeaderCode:77420311516028caed1262-45097259%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d9841eba45f7b8a04408b709143683389d08b88' => 
    array (
      0 => '/var/www/att/web/views/index/api.tpl',
      1 => 1365256393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '77420311516028caed1262-45097259',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'pfad' => 0,
    'antrage' => 0,
    'details' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_516028caf3c481_05472026',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_516028caf3c481_05472026')) {function content_516028caf3c481_05472026($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('../template/top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Anträge</h1>
        <p>Wilkommen auf dem Antragstool der Jungen Piraten. Hier kannst du Online dir die Anträge des Bundesvorstandes und einiger Landesvorstände einsehen und neue stellen.</p>
        <p><a href="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
antrag/neu" class="btn btn-primary btn-large">Neuen Antrag &raquo;</a></p>
      </div>
      <div id="row">
        <div class="span12">
          <h3>Die letzten Anträge:</h3>
          <table style="width:100%;">
            <tr>
              <th>Antrags Name</th>
              <th>Gliederung</th>
              <th>Status</th>
              <th>Info</th>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['details'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['details']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['antrage']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['details']->key => $_smarty_tpl->tpl_vars['details']->value){
$_smarty_tpl->tpl_vars['details']->_loop = true;
?>
            <tr>
              <td><a href="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo $_tmp2;?>
antrag/antrag/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['details']->value["id"];?>
<?php $_tmp3=ob_get_clean();?><?php echo $_tmp3;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['details']->value["name"];?>
<?php $_tmp4=ob_get_clean();?><?php echo $_tmp4;?>
</a></td>
              <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['details']->value["gliederung"];?>
<?php $_tmp5=ob_get_clean();?><?php echo $_tmp5;?>
</td>
              <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['details']->value["status"];?>
<?php $_tmp6=ob_get_clean();?><?php echo $_tmp6;?>
</td>
              <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['details']->value["info"];?>
<?php $_tmp7=ob_get_clean();?><?php echo $_tmp7;?>
</td>
            </tr>
            <?php } ?>
          </table>
        </div>
      </div>
      <!-- Example row of columns -->
      <!--
      <div class="row">
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
       </div>
        <div class="span4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
      </div>
      !-->

     
<?php echo $_smarty_tpl->getSubTemplate ('../template/bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>