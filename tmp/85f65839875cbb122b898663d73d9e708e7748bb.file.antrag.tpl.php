<?php /* Smarty version Smarty-3.1.13, created on 2013-04-19 18:51:52
         compiled from "/var/www/att/web/views/antrag/antrag.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18060892615160216dab6e61-09664299%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85f65839875cbb122b898663d73d9e708e7748bb' => 
    array (
      0 => '/var/www/att/web/views/antrag/antrag.tpl',
      1 => 1366389940,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18060892615160216dab6e61-09664299',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5160216dbd9ea5_62484941',
  'variables' => 
  array (
    'antrag' => 0,
    'berechtigung' => 0,
    'nutzerName' => 0,
    'personVoting' => 0,
    'boardID' => 0,
    'threadid' => 0,
    'voting' => 0,
    'vote' => 0,
    'user' => 0,
    'u' => 0,
    'log' => 0,
    'logItem' => 0,
    'status' => 0,
    'stati' => 0,
    'pfad' => 0,
    'id' => 0,
    'gliederungWiki' => 0,
    'antragsTime' => 0,
    'token' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5160216dbd9ea5_62484941')) {function content_5160216dbd9ea5_62484941($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/att/vendor/smarty/libs/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ('../template/top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



    <div class="container">

     <div class="hero-unit">
        <h1><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["name"];?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
</h1>
        <p><b>Antrag:</b><br><pre style="font-size:20px;"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["beschluss"];?>
<?php $_tmp2=ob_get_clean();?><?php echo $_tmp2;?>
</pre></p>
		<hr>
		<p><b>Begründung:</b><br><pre style="font-size:20px;"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["begruendung"];?>
<?php $_tmp3=ob_get_clean();?><?php echo $_tmp3;?>
</pre></p>
		<?php if ($_smarty_tpl->tpl_vars['berechtigung']->value==true){?>
		<p>
			
			<div class="btn-group" data-toggle="buttons-radio">
              	<button type="button" voteName="" vote="ja" class="btn btn-success vote <?php if ($_smarty_tpl->tpl_vars['personVoting']->value[$_smarty_tpl->tpl_vars['nutzerName']->value]=="ja"){?>active<?php }?>">Dafür</button>
				<button type="button" voteName="" vote="enthaltung" class="btn btn-warning vote <?php if ($_smarty_tpl->tpl_vars['personVoting']->value[$_smarty_tpl->tpl_vars['nutzerName']->value]=="enthaltung"){?>active<?php }?>">Enthaltung</button>
				<button type="button" voteName="" vote="nein" class="btn btn-danger vote <?php if ($_smarty_tpl->tpl_vars['personVoting']->value[$_smarty_tpl->tpl_vars['nutzerName']->value]=="nein"){?>active<?php }?>">Dagegen</button>
            </div>
		</p>

		<?php }?>
      </div>
	<div class="row">
	<div class="span9">
	<ul id="myTab" class="nav nav-tabs">
              <li  class="active"><a href="#kommentare" data-toggle="tab">Komentare</a></li>
              <li><a href="#beschluss" data-toggle="tab">Abgestimmt</a></li>
              <?php if ($_smarty_tpl->tpl_vars['berechtigung']->value){?><li><a href="#abstimmung" data-toggle="tab">Abstimmen</a></li><?php }?>
			  <li><a href="#log" data-toggle="tab">Beschluss-Log</a></li>
			  <?php if ($_smarty_tpl->tpl_vars['berechtigung']->value){?><li><a href="#admin" data-toggle="tab">Beschluss-Treffen</a></li><?php }?>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade in active" id="kommentare">
                <p>Die Diskussion zu diesem Antrag findet im Forum statt:</p>
                <a href="https://forum.junge-piraten.de/viewthread.php?boardid=<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['boardID']->value;?>
<?php $_tmp4=ob_get_clean();?><?php echo $_tmp4;?>
&threadid=<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['threadid']->value;?>
<?php $_tmp5=ob_get_clean();?><?php echo $_tmp5;?>
">Forum</a>
              </div>
              <div class="tab-pane fade" id="beschluss">
                <p>
					<table class="table" style="width:50%;">
						<p>So wurde abgestimmt von:</p>
						<tr><th>Name</th><th>Abgestimmt</th></tr>
						<?php  $_smarty_tpl->tpl_vars['vote'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['vote']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['voting']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['vote']->key => $_smarty_tpl->tpl_vars['vote']->value){
$_smarty_tpl->tpl_vars['vote']->_loop = true;
?>
								
									<?php if ($_smarty_tpl->tpl_vars['vote']->value["voting"]=="ja"){?>
										<tr><td ><a href="https://wiki.junge-piraten.de/wiki/Benutzer:<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vote']->value["nutzerName"];?>
<?php $_tmp6=ob_get_clean();?><?php echo $_tmp6;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vote']->value["nutzerName"];?>
<?php $_tmp7=ob_get_clean();?><?php echo $_tmp7;?>
</a></td><td><a style="width:70px;cursor:text;" class="btn btn-success result active">Dafür</a></td></tr>
									<?php }?>
									<?php if ($_smarty_tpl->tpl_vars['vote']->value["voting"]=="nein"){?>
										<tr><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vote']->value["nutzerName"];?>
<?php $_tmp8=ob_get_clean();?><?php echo $_tmp8;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vote']->value["nutzerName"];?>
<?php $_tmp9=ob_get_clean();?><?php echo $_tmp9;?>
</a></td><td><a style="width:70px;cursor:text;" class="btn btn-danger result active">Dagegen</a></td></tr>
									<?php }?>
									<?php if ($_smarty_tpl->tpl_vars['vote']->value["voting"]=="enthaltung"){?>
										<tr><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vote']->value["nutzerName"];?>
<?php $_tmp10=ob_get_clean();?><?php echo $_tmp10;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vote']->value["nutzerName"];?>
<?php $_tmp11=ob_get_clean();?><?php echo $_tmp11;?>
</a></td><td><a style="width:70px;cursor:text;" class="btn btn-warning result active">Enthaltung</a></td></tr>
									<?php }?>
						<?php } ?>
					</table>
				</p>
              </div>
              <?php if ($_smarty_tpl->tpl_vars['berechtigung']->value){?>
              <div class="tab-pane fade" id="abstimmung">
              	<p>Hier kannst du im Namen von andere Vorstandsmitglieder abstimmen.</p>
              <table class="table">
              <tr><th>Name</th><th>Abstimmung</th></tr>
              <?php  $_smarty_tpl->tpl_vars['u'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['u']->key => $_smarty_tpl->tpl_vars['u']->value){
$_smarty_tpl->tpl_vars['u']->_loop = true;
?>
              		<tr>
              			<td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['u']->value;?>
<?php $_tmp12=ob_get_clean();?><?php echo $_tmp12;?>
</td>
              			<td>
              				<div class="btn-group" data-toggle="buttons-radio">
              						<button type="button" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="ja" class="btn btn-success vote <?php if ($_smarty_tpl->tpl_vars['personVoting']->value[$_smarty_tpl->tpl_vars['u']->value]=="ja"){?>active<?php }?>">Dafür</button>
									<button type="button" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="enthaltung" class="btn btn-warning vote <?php if ($_smarty_tpl->tpl_vars['personVoting']->value[$_smarty_tpl->tpl_vars['u']->value]=="enthaltung"){?>active<?php }?>">Enthaltung</button>
									<button type="button" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="nein" class="btn btn-danger vote <?php if ($_smarty_tpl->tpl_vars['personVoting']->value[$_smarty_tpl->tpl_vars['u']->value]=="nein"){?>active<?php }?>">Dagegen</button>
              				</div>
	              			<!--<a class="btn btn-success vote" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="ja">Dafür</a>
							<a class="btn btn-warning vote" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="enthaltung">Enthaltung</a>
							<a class="btn btn-danger vote" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="nein">Dagegen</a>!-->
						</td>
					</tr>
              <?php } ?>
              </table>
              </div>
              <?php }?>
				<div class="tab-pane fade" id="log">
					<p>Hier siest du die Abstimmungs History:</p>
                <p>
					<table class="table">
						<?php  $_smarty_tpl->tpl_vars['logItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['logItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['log']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['logItem']->key => $_smarty_tpl->tpl_vars['logItem']->value){
$_smarty_tpl->tpl_vars['logItem']->_loop = true;
?>
								<tr><td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['logItem']->value["time"],"%d.%m.%Y - %H:%M");?>
</td><td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['logItem']->value["log"];?>
<?php $_tmp13=ob_get_clean();?><?php echo $_tmp13;?>
</td></tr>
						<?php } ?>
					</table>
				</p>
              </div>
              <?php if ($_smarty_tpl->tpl_vars['berechtigung']->value){?>
				<div class="tab-pane fade" id="admin">
					<p>Hier kannst du das Ergebniss festlegen und den Status ändern</p>
				<b>Ergebiss:</b><br>
				<div class="btn-group" data-toggle="buttons-radio">
              						<button type="button" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="angenommen" class="btn btn-success result <?php if ($_smarty_tpl->tpl_vars['antrag']->value["info"]=="angenommen"){?>active<?php }?>">Angenommen</button>
									<button type="button" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="abstimmung" class="btn btn-warning result <?php if ($_smarty_tpl->tpl_vars['antrag']->value["info"]=="abstimmung"){?>active<?php }?>">Abstimmung</button>
									<button type="button" voteName="<?php echo $_smarty_tpl->tpl_vars['u']->value;?>
" vote="abgelehnt" class="btn btn-danger result <?php if ($_smarty_tpl->tpl_vars['antrag']->value["info"]=="abgelehnt"){?>active<?php }?>">Abgelehnt</button>
              </div><br><br>
				<b>Status:</b><br>
				<select id="status">
					<?php  $_smarty_tpl->tpl_vars['stati'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stati']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['status']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stati']->key => $_smarty_tpl->tpl_vars['stati']->value){
$_smarty_tpl->tpl_vars['stati']->_loop = true;
?>
						<option <?php if ($_smarty_tpl->tpl_vars['stati']->value==$_smarty_tpl->tpl_vars['antrag']->value["status"]){?> selected <?php }?>><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['stati']->value;?>
<?php $_tmp14=ob_get_clean();?><?php echo $_tmp14;?>
</option>
					<?php } ?>
				</select>
				<script language="javascript">
				$("#status").change( function() {
					window.location.href = "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp15=ob_get_clean();?><?php echo $_tmp15;?>
antrag/status/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['id']->value;?>
<?php $_tmp16=ob_get_clean();?><?php echo $_tmp16;?>
/"+$(this).val();
				});
				</script>
				<?php ob_start();?><?php if ($_smarty_tpl->tpl_vars['antrag']->value["gliederung"]=="Bund"){?><?php $_tmp17=ob_get_clean();?><?php echo $_tmp17;?>

				<h3>Admin</h3>
				<a href="#" id="pushToWiki">Push To Wiki</a>
				<?php ob_start();?><?php }?><?php $_tmp18=ob_get_clean();?><?php echo $_tmp18;?>

				</div>
				<?php }?>
            </div>

	</div>
<div class="span3">
<table class="table table-bordered">
<tr><td>ID</td><td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["antragsID"];?>
<?php $_tmp19=ob_get_clean();?><?php echo $_tmp19;?>
</td></tr>
<tr><td>Gliederung</td><td><?php ob_start();?><?php if ($_smarty_tpl->tpl_vars['gliederungWiki']->value["wikiSeite"]!=null){?><?php $_tmp20=ob_get_clean();?><?php echo $_tmp20;?>
<a href="https://wiki.junge-piraten.de/wiki/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['gliederungWiki']->value["wikiSeite"];?>
<?php $_tmp21=ob_get_clean();?><?php echo $_tmp21;?>
"><?php }?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["gliederung"];?>
<?php $_tmp22=ob_get_clean();?><?php echo $_tmp22;?>
<?php ob_start();?><?php if ($_smarty_tpl->tpl_vars['gliederungWiki']->value["wikiSeite"]!=null){?><?php $_tmp23=ob_get_clean();?><?php echo $_tmp23;?>
</a><?php }?></td></tr>
<tr><td>Budget</td><td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["budget"];?>
<?php $_tmp24=ob_get_clean();?><?php echo $_tmp24;?>
</td></tr>
<tr><td>Status</td><td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["status"];?>
<?php $_tmp25=ob_get_clean();?><?php echo $_tmp25;?>
</td></tr>
<tr><td>Ergebnis</td><td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["info"];?>
<?php $_tmp26=ob_get_clean();?><?php echo $_tmp26;?>
</td></tr>
<tr><td>Autor</td><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["antragssteller"];?>
<?php $_tmp27=ob_get_clean();?><?php echo $_tmp27;?>
"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["antragssteller"];?>
<?php $_tmp28=ob_get_clean();?><?php echo $_tmp28;?>
</a></td></tr>
<?php if (isset($_smarty_tpl->tpl_vars['antragsTime']->value["gestellt"])){?>
<tr><td>Datum der Antragsstellung</td><td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['antragsTime']->value["gestellt"],"%d.%m.%Y - %H:%M");?>
</td></tr>
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['antragsTime']->value["beschluss"])){?>
<tr><td>Datum der Entscheidung</td><td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['antragsTime']->value["beschluss"],"%d.%m.%Y - %H:%M");?>
</td></tr>
<?php }?>
</table>
</div>
	</div>
<script language="javascript">
function reloadBeschluss()
{
	$.ajax({
		type: "GET",
		url: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp29=ob_get_clean();?><?php echo $_tmp29;?>
api/antrag/vote?id=<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["id"];?>
<?php $_tmp30=ob_get_clean();?><?php echo $_tmp30;?>
"
	}).done(function( msg ) {
		var obj = jQuery.parseJSON(msg);
		var text = '<p>So wurde abgestimmt von:</p><table class="table" style="width:50%;">';
		$.map(obj, function(value, key) {
			//text = text + "<tr><td>"+key+"</td><td>"+value+"</td></tr>";
			text = text + '<tr><td><a href="https://wiki.junge-piraten.de/wiki/Benutzer:'+key+'">'+key+'</a></td>';
			if(value=="ja")
			{
				text = text + '<td><a style="width:70px;cursor:text;" class="btn btn-success result active">Dafür</a></td></tr>';
			}
			if(value=="nein")
			{
				text = text + '<td><a style="width:70px;cursor:text;" class="btn btn-danger result active">Dagegen</a></td></tr>';
			}
			if(value=="enthaltung")
			{
				text = text + '<td><a style="width:70px;cursor:text;" class="btn btn-warning result active">Enthaltung</a></td></tr>';
			}
		});
		text = text + "</tabel>";
		$("#beschluss").html(text);
			//alert("Abgestimmt");
	});
	window.setTimeout("reloadBeschluss()", 5000);
}
</script>
<?php if ($_smarty_tpl->tpl_vars['berechtigung']->value==true){?>
<script lanugage="javascript">
$(".vote").click( function() {
	$.ajax({
	type: "PUT",
		url: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp31=ob_get_clean();?><?php echo $_tmp31;?>
api/antrag/vote",
		data: { token: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['token']->value;?>
<?php $_tmp32=ob_get_clean();?><?php echo $_tmp32;?>
", stimme: $(this).attr("vote"), antrag: <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["id"];?>
<?php $_tmp33=ob_get_clean();?><?php echo $_tmp33;?>
, name: $(this).attr("voteName")  }
		}).done(function( msg ) {
			alert("Abstimmung erfolgreich");
			reloadBeschluss();
		});
});
$(".result").click( function() {
	$.ajax({
	type: "PUT",
		url: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp34=ob_get_clean();?><?php echo $_tmp34;?>
api/antrag/result",
		data: { token: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['token']->value;?>
<?php $_tmp35=ob_get_clean();?><?php echo $_tmp35;?>
", result: $(this).attr("vote"), antrag: <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["id"];?>
<?php $_tmp36=ob_get_clean();?><?php echo $_tmp36;?>
  }
		}).done(function( msg ) {
			location.reload();
		});
});
$("#status").change( function() {
	$.ajax({
	type: "PUT",
		url: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp37=ob_get_clean();?><?php echo $_tmp37;?>
api/antrag/status",
		data: { token: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['token']->value;?>
<?php $_tmp38=ob_get_clean();?><?php echo $_tmp38;?>
", status: $(this).val(), antrag: <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["id"];?>
<?php $_tmp39=ob_get_clean();?><?php echo $_tmp39;?>
  }
		}).done(function( msg ) {
			location.reload();
		});
});
$("#pushToWiki").click (function () {
	$.ajax({
	type: "PUT",
		url: "<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['pfad']->value;?>
<?php $_tmp40=ob_get_clean();?><?php echo $_tmp40;?>
api/beta/pushToWiki",
		data: { antrag: <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['antrag']->value["id"];?>
<?php $_tmp41=ob_get_clean();?><?php echo $_tmp41;?>
  }
		}).done(function( msg ) {
			console.log(msg);
			alert("Wikiseite erstellt (beta)");
		});
});
</script>
<?php }?>
     
<?php echo $_smarty_tpl->getSubTemplate ('../template/bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>